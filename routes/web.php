<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Address;

use App\Post;

use App\Role;

use App\Photo;
use App\Staff;
use App\Product;

use App\Tag;
use App\Video;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//----------------------------------------------------
//visualizando la relacion 1 a 1 de user address
//-----------------------------------------------------

//insert una direccion para el usuario
Route::get('/onetoone/insert', function(){
    $user = User::findOrFail(1);
    $address = new Address(['name' => 'portal de belen']);
    $user->address()->save($address);
});

//editar la direccion del usuario
Route::get('/onetoone/update', function(){
    $address = Address::where('user_id', 1)->first(); 
    $address->name = 'casa rosada'; 
    $address->save();
});

//ver la direccion del usuario
Route::get('/onetoone/read', function(){
    $user = User::findOrFail(1); 
    return $user->address->name;
}); 

//eliminar la direccion del usuario
Route::get('/onetoone/delete', function(){
    $user = User::findOrFail(1); 
    $user->address->delete();
});


//----------------------------------------------------
//visualizando la relacion 1 a N de user address
//-----------------------------------------------------


//crear una publicion de usuario
Route::get('/onetomany/create', function(){
    $user = User::findOrFail(1);
    $post = new Post(['title' => 'created posting', 'body' => 'this is content']);
    $user->post()->save($post);
});

//editar una publicacion del usuario
Route::get('/onetomany/update', function(){
    $user = User::findOrFail(1); 
    $user->post()->where('Id', 1)->update(['title' => 'editing post', 'body' => 'this is edited']);
});

//ver publicaciones del usuario
Route::get('/onetomany/read', function(){
    $user = User::findOrFail(1); 
    foreach ($user->post as $post => $value) {
        echo $value->title . ', ' . $value->body . '<br>';
    }
}); 

//eliminar la direccion del usuario
Route::get('/onetomany/delete', function(){
    $user = User::findOrFail(1); 
    $user->post()->where('Id', 1)->delete();
});

//----------------------------------------------------
//visualizando la relacion N a N de user roles
//-----------------------------------------------------

//agregar rol a usuario
Route::get('/manytomany/create', function(){
    $user = User::findOrFail(1);
    $role = new Role(['name' => 'Admin']);
    $user->roles()->save($role);
});

//editar rol de usuario
Route::get('/manytomany/update', function(){
    $user = User::findOrFail(1);
    if($user->has('roles')){
        foreach ($user->roles as $role) {
            if($role->name == 'Administrador'){
                $role->name = 'Client';
                $role->save();
            }
        } 
    } 
    
});

//ver roles del usuario
Route::get('/manytomany/read', function(){
    $user = User::findOrFail(1); 
    foreach ($user->roles as $role => $value) {
        echo $value->name . '<br>';
    }
}); 

//eliminar rol de usuario
Route::get('/manytomany/delete', function(){
    $user = User::findOrFail(1);
    if($user->has('roles')){
        foreach ($user->roles as $role) {
            $role->where('Id',2)->delete();
        } 
    }  
});

//asignar rol a usuario
Route::get('/attach', function(){
    $user = User::findOrFail(1);
    $user->roles()->attach(3);
});

//desasignar rol a usuario
Route::get('/detach', function(){
    $user = User::findOrFail(1);
    $user->roles()->detach(3);
});

//Sincronizar los roles de una usuario
Route::get('/sync', function(){
    $user = User::findOrFail(1);
    $user->roles()->sync([1]);
});


//-----------------------
//Relaciones polimorficas
//-----------------------

//Mostrando relación 1 a 1
//create photo para un staff
Route::get('/morph/create', function(){
    $staff = Staff::find(2);
    $staff->photos()->create(['path' => 'prueba_para_staff.jpg']);
});

//ver photo de un staff
Route::get('/morph/read', function(){
    $staff = Staff::find(2);
    foreach ($staff->photos as $photo) {
        echo $photo->path . '<br>';
    } 
});

//editar photo de un staff
Route::get('/morph/update', function(){
    $staff = Staff::find(2);
    $staff->photos()->where('Id', 1)->first()->update(['path' => 'prueda_editar_para_staff.jpg']);
});

//eliminar photo de un staff
Route::get('/morph/delete', function(){
    $staff = Staff::find(2);
    $staff->photos()->where('Id', 1)->delete();
});

//asignar photo de un staff
Route::get('/morph/assing', function(){
    $staff = Staff::findOrFail(2);
    $photo = Photo::findOrFail(3);
    $staff->photos()->save($photo);
});

//desasignar photo de un staff
Route::get('/morph/des-assing', function(){
    $staff = Staff::findOrFail(2);
    $staff->photos()->where('id', 3)->update(['imageable_id' => 0, 'imageable_type' => '']);
});

//Mostrando relación N a N
//insertando post y agregando relacion con tag, así mismo video y se le asigna tag
Route::get('/morph/manytomany/insert', function(){
    $post = Post::create(['title' => 'morph many to many example', 'body' => 'first example']);
    $tagForPost = Tag::find(1);
    $post->tags()->save($tagForPost);

    $video = Video::create(['name' => 'eloquent basic.mp4']);
    $tagForVideo = Tag::find(2);
    $video->tags()->save($tagForVideo);
});

//Mostrando tags de un video y de un post
Route::get('/morph/manytomany/read', function(){ 
    $post = Post::findOrFail(4); 
    echo 'Post <br>';
    foreach ($post->tags as $tag) {
        echo $tag->name . '<br>';
    }
    $video = Video::findOrFail(1); 
    echo '<br> Video <br>';
    foreach ($video->tags as $tag) {
        echo $tag->name . '<br'; 
    }
});

//Editar tag de un video o post
Route::get('/morph/manytomany/update', function(){ 
    $post = Post::findOrFail(4);  
    foreach ($post->tags as $tag) {
        return $tag->where('name', 'laravel')->update(['name' => 'updated laravel']);
    }
    /* $video = Video::findOrFail(1);  
    foreach ($video->tags as $tag) {
        return $tag->where('name', 'php')->update(['name', 'updated php']);
    } */
});

//eliminar tag de un video o post
Route::get('/morph/manytomany/delete', function(){ 
    $post = Post::findOrFail(4);  
    foreach ($post->tags as $tag) {
        return $tag->where('id', 1)->delete();
    }
    /* $video = Video::findOrFail(1);  
    foreach ($video->tags as $tag) {
        return $tag->where('id', 2)->delete();
    } */
});