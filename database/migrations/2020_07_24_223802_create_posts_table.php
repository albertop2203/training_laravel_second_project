<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{ 
    //creación de la table posts
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned()->nullable()->index();// id user para la relacion entre modelos
            $table->string('title');
            $table->string('body');
            $table->timestamps();
        });
    } 
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
