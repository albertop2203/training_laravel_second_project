<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{ 
    //tabla para hacer el ejercicio polimorficas 1 a 1 con fotos
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
    } 
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
