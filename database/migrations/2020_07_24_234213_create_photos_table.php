<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotosTable extends Migration
{ 
    //Tabla para guardar los registros 1 a 1 polimorficas
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->id();
            $table->string('path'); //nombre del archivo 
            $table->string('imageable_type'); //variable para almacenar que tipo de modelo es
            $table->integer('imageable_id'); // id del modela que se guarda en imageable_type
            $table->timestamps();
        });
    } 
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
