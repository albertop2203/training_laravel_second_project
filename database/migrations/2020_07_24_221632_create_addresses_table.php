<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{ 
    //creacion de la table addresses
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('user_id')->unsigned()->nullable()->index(); // id user para la relacion entre modelos
            $table->timestamps();
        });
    } 
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
