<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleUserTable extends Migration
{ 
    //creación de tabla intermedia entre roles y usuario para la relación N a N
    public function up()
    {
        Schema::create('role_user', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned()->nullable()->index();// id user para la relacion con el usuario
            $table->integer('role_id')->unsigned()->nullable()->index();// id role para la relacion con el rol 
            $table->timestamps();
        });
    } 
    public function down()
    {
        Schema::dropIfExists('role_user');
    }
}
