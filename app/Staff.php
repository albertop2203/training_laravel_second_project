<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = [
        'name'
    ];
    //relacion 1 a muchos con photos
    public function photos(){
        return $this->morphMany('App\photo', 'imageable');
    }
}
