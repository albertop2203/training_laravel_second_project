<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //propiedades para el modelo video
    protected $fillable = [
        'name'
    ];
    //relación polimorficas con tags N a N
    public function tags(){
        return $this->morphToMany('App\Tag', 'taggable');
    }
}
