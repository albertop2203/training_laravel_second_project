<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //propiedades para el manejo del modelo Post
    protected $fillable = [
        'title',
        'body'
    ];
    //relación polimorficas con tags N a N
    public function tags(){
      return $this->morphToMany('App\Tag', 'taggable');
    }
}
