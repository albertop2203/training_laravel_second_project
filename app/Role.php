<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
     //propiedades para el manejo del modelo Role
    protected $fillable = [
        'name'
    ];
}
