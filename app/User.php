<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable; 
    protected $fillable = [
        'name', 'email', 'password',
    ]; 
    protected $hidden = [
        'password', 'remember_token',
    ]; 
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //relacion 1 a 1 con address
    public function address(){
        return $this->hasOne('App\Address');
    }

    //relación 1 a N con Post
    public function post(){
        return $this->hasMany('App\Post');
    }

    //relación N a N con Roles
    public function Roles(){
        return $this->belongsToMany('App\Role');
    }
}
