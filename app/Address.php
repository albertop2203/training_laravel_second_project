<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //propiedades para el manejo del model Address
    protected $fillable = [
        'name'
    ];
}
